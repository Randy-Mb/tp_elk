const fs = require('fs')
const elasticbulk = require('elasticbulk');

function csvToJSON(csv) {
    var lines = csv.split("\n");

    var result = [];

    var headers = ['title', 'seo_title', 'url', 'author', 'date', 'category', 'locales', 'content'];

    for (var i = 0; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(";");

        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }

        result.push(obj);

    }

    return JSON.stringify(result, null, '\t');
}

const csv = fs.readFileSync('Data.csv').toString();
const json = JSON.parse(csvToJSON(csv));
//console.log(json);
elasticbulk.import(json, {
    index: 'groupe9',
    type: 'TP',
    host: 'http://localhost:9200/',
}).finally(
    console.log('Importation terminé.')
)
